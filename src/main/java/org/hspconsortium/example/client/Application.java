package org.hspconsortium.example.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@ComponentScan(
        basePackages = {"org.hspconsortium.example.client", "org.hspconsortium.client"},
        excludeFilters = @ComponentScan.Filter(SpringBootApplication.class))
@EnableWebMvc
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
