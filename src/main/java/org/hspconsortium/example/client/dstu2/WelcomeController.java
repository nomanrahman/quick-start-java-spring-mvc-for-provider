package org.hspconsortium.example.client.dstu2;

import ca.uhn.fhir.model.dstu2.composite.QuantityDt;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import org.apache.commons.lang.StringUtils;
import org.hspconsortium.client.auth.credentials.ClientSecretCredentials;
import org.hspconsortium.client.session.Session;
import org.hspconsortium.client.session.authorizationcode.AuthorizationCodeSessionFactory;
import org.hspconsortium.example.client.model.MyHeight;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
@Profile("dstu2")
public class WelcomeController {

    @Inject
    private AuthorizationCodeSessionFactory<ClientSecretCredentials> ehrSessionFactory;

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String patientHeightChart(HttpSession httpSession, Model model) {
                // retrieve the EHR session from the http session
        Session ehrSession = (Session) httpSession.getAttribute(ehrSessionFactory.getSessionKey());

        Collection<Observation> heightObservations = ehrSession
                .getContextDSTU2()
                .getPatientContext()
                .find(Observation.class, Observation.CODE.exactly().identifier("8302-2"));

        Patient patient = ehrSession.getContextDSTU2().getPatientResource();

        model.addAttribute("title", "DSTU2 Example");

        model.addAttribute("patientFullName", StringUtils.join(patient.getName().get(0).getGiven(), " ") + " " +
                        patient.getName().get(0).getFamily().get(0));

        List<MyHeight> heights = new ArrayList<>();
        for (Observation heightObservation : heightObservations) {
            String date = ((DateTimeDt)heightObservation.getEffective()).getValue().toString();
            String height = ((QuantityDt)heightObservation.getValue()).getValue().toString();
            heights.add(new MyHeight(height, date));
        }

        model.addAttribute("heights", heights);
        return "example";
    }

}
