package org.hspconsortium.example.client.model;

public class MyHeight {
    private final String height;
    private final String date;

    public MyHeight( String height, String date ) {
        this.height = height;
        this.date = date;
    }

    public String getHeight() {
        return height;
    }

    public String getDate() {
        return date;
    }
}
