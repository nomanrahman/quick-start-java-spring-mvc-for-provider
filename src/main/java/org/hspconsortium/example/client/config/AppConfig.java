/*
 * #%L
 * hspc-client-example
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package org.hspconsortium.example.client.config;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.IRestfulClientFactory;
import org.hspconsortium.client.auth.StateProvider;
import org.hspconsortium.client.auth.access.AccessTokenProvider;
import org.hspconsortium.client.auth.access.JsonAccessTokenProvider;
import org.hspconsortium.client.auth.authorizationcode.AuthorizationCodeRequestBuilder;
import org.hspconsortium.client.auth.credentials.ClientSecretCredentials;
import org.hspconsortium.client.controller.FhirEndpointsProvider;
import org.hspconsortium.client.session.ApacheHttpClientFactory;
import org.hspconsortium.client.session.FhirSessionContextHolder;
import org.hspconsortium.client.session.SessionKeyRegistry;
import org.hspconsortium.client.session.authorizationcode.AuthorizationCodeSessionFactory;
import org.hspconsortium.client.session.impl.SimpleFhirSessionContextHolder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.inject.Inject;

@Configuration
public class AppConfig {

    @Value("${example.clientId}")
    private String clientId;

    @Value("${example.scope}")
    private String scope;

    @Value("${example.redirectUri}")
    private String redirectUri;

    @Value("${example.clientSecret}")
    private String clientSecret;

    @Value("${example.appEntryPoint}")
    private String appEntryPoint;

    @Value("${example.httpConnectionTimeoutMilliSeconds:}")
    private String httpConnectionTimeoutMilliSeconds;

    @Value("${example.httpReadTimeoutMilliSeconds:}")
    private String httpReadTimeoutMilliSeconds;

    @Bean
    public String clientId() {
        return clientId;
    }

    @Bean
    public String scope() {
        return scope;
    }

    @Bean
    public String redirectUri() {
        return redirectUri;
    }

    @Bean
    public String clientSecret() {
        return clientSecret;
    }

    @Bean
    public String appEntryPoint() {
        return appEntryPoint;
    }

    @Bean
    public Integer httpConnectionTimeOut() {
        return Integer.parseInt(
                httpConnectionTimeoutMilliSeconds != null && httpConnectionTimeoutMilliSeconds.length() > 0
                        ? httpConnectionTimeoutMilliSeconds
                        : IRestfulClientFactory.DEFAULT_CONNECT_TIMEOUT + "");
    }

    @Bean
    public Integer httpReadTimeOut() {
        return Integer.parseInt(
                httpReadTimeoutMilliSeconds != null && httpReadTimeoutMilliSeconds.length() > 0
                        ? httpReadTimeoutMilliSeconds
                        : IRestfulClientFactory.DEFAULT_CONNECTION_REQUEST_TIMEOUT + "");
    }

    @Bean
    public String proxyPassword() {
        return System.getProperty("http.proxyPassword", System.getProperty("https.proxyPassword"));
    }

    @Bean
    public String proxyUser() {
        return System.getProperty("http.proxyUser", System.getProperty("https.proxyUser"));
    }

    @Bean
    public Integer proxyPort() {
        return Integer.parseInt(System.getProperty("http.proxyPort", System.getProperty("https.proxyPort", "8080")));
    }

    @Bean
    public String proxyHost() {
        //To Use With Proxy
        //-Dhttp.proxyHost=proxy.host.com -Dhttp.proxyPort=8080  -Dhttp.proxyUser=username -Dhttp.proxyPassword=password
        return System.getProperty("http.proxyHost", System.getProperty("https.proxyHost"));
    }

    @Bean
    public StateProvider stateProvider() {
        return new StateProvider.DefaultStateProvider();
    }

    @Bean
    public FhirSessionContextHolder fhirSessionContextHolder() {
        return new SimpleFhirSessionContextHolder();
    }

    @Bean
    @Inject
    public ApacheHttpClientFactory apacheHttpClientFactory(String proxyHost,
                                                           Integer proxyPort,
                                                           String proxyUser,
                                                           String proxyPassword,
                                                           Integer httpConnectionTimeOut,
                                                           Integer httpReadTimeOut) {
        return new ApacheHttpClientFactory(proxyHost, proxyPort, proxyUser, proxyPassword,
                httpConnectionTimeOut, httpReadTimeOut);
    }

    @Bean
    public AccessTokenProvider accessTokenProvider(ApacheHttpClientFactory apacheHttpClientFactory) {
        return new JsonAccessTokenProvider(apacheHttpClientFactory);
    }

    @Bean
    @Inject
    public AuthorizationCodeRequestBuilder authorizationCodeRequestBuilder(FhirEndpointsProvider fhirEndpointsProvider,
                                                                           StateProvider stateProvider) {
        return new AuthorizationCodeRequestBuilder(fhirEndpointsProvider, stateProvider);
    }

    @Bean
    @Inject
    public ClientSecretCredentials clientSecretCredentials(String clientSecret) {
        return new ClientSecretCredentials(clientSecret);
    }

    @Bean
    public SessionKeyRegistry sessionKeyRegistry() {
        return new SessionKeyRegistry();
    }

    @Bean
    @Inject
    public AuthorizationCodeSessionFactory<ClientSecretCredentials>
    authorizationCodeSessionFactory(FhirContext fhirContext, SessionKeyRegistry sessionKeyRegistry,
                                    FhirSessionContextHolder fhirSessionContextHolder,
                                    AccessTokenProvider patientAccessTokenProvider,
                                    String clientId, ClientSecretCredentials clientSecretCredentials, String redirectUri) {
        return new AuthorizationCodeSessionFactory<>(fhirContext, sessionKeyRegistry, "MySessionKey", fhirSessionContextHolder,
                patientAccessTokenProvider, clientId, clientSecretCredentials, redirectUri);
    }

}
