/*
 * #%L
 * hspc-client-example
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package org.hspconsortium.example.client.stu3;

import ca.uhn.fhir.context.FhirContext;
import org.hspconsortium.client.controller.FhirEndpointsProvider;
import org.hspconsortium.client.controller.FhirEndpointsProviderSTU3;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("stu3")
public class FHIRConfig {

    @Bean
    public FhirEndpointsProvider fhirEndpointsProvider(FhirContext fhirContext) {
        return new FhirEndpointsProviderSTU3(fhirContext);
    }

    @Bean
    public FhirContext fhirContext(Integer httpConnectionTimeOut, Integer httpReadTimeOut, String proxyHost,
                                   Integer proxyPort, String proxyUser, String proxyPassword) {
        FhirContext hapiFhirContext = FhirContext.forDstu3();
        // Set how long to try and establish the initial TCP connection (in ms)
        hapiFhirContext.getRestfulClientFactory().setConnectTimeout(httpConnectionTimeOut);

        // Set how long to block for individual read/write operations (in ms)
        hapiFhirContext.getRestfulClientFactory().setSocketTimeout(httpReadTimeOut);

        if (proxyHost != null) {
            hapiFhirContext.getRestfulClientFactory().setProxy(proxyHost, proxyPort);

            hapiFhirContext.getRestfulClientFactory().setProxyCredentials(proxyUser
                    , proxyPassword);
        }
        return hapiFhirContext;
    }

}
