## Introduction

This guide walks you through the process of creating a simple Java webapp using Spring MVC for a healthcare provider (doctor, nurse, pharmacist, etc.) that is launched from an EHR (HSPC Sandbox) and queries the height observations for the patient in the EHR context. This simple webapp is a great starter project from which you can begin to add your services, business logic and user interface.

## Prerequisites

* An account on the HSPC Sandbox
* Understanding of the [HSPC Java Client](https://bitbucket.org/hspconsortium/java-client)
* (Optional) Familiarity with Spring MVC
* (Optional) Familiarity with the SMART on FHIR specification
* (Optional) Familiarity with the SMART Confidential Client
* (Optional) Familiarity with the HAPI query model

## What you will build

In this guide, you will build a simple Java Spring MVC webapp that consists of a JSP, Controller, and config files.  If you prefer, you can skip the steps in the guide and download a finished copy of the project.

## What you will need

* An IDE or text editor
* Java
* Maven 

## Running the Quick Start

### Step 1: Configure the App (choose either DSTU2 or STU3)

The Quick Start can be run in either DSTU2 or STU3 mode.  To select a mode, modify the **application.yml** to activate the desired Spring Profile.

    spring:
      profiles:
        active: dstu2

or

    spring:
      profiles:
        active: stu3

### Step 2: Define the appEntryPoint

Next, specify the controller that will be sent the request following completion of the SMART on FHIR launch flow.  The HSPC Java Client will handle the redirect, then send a redirect to the appEntryPoint URL:

    example:
      appEntryPoint: http://localhost:8080/welcome

Since we are providing an appEntryPoint bean that contains a context of "/welcome", we must provide a controller at that path.  Our controller will use the HSPC Java Client session and query the EHR using an extension of the HAPI fluent query language:

** dstu2/WelcomeController.java **

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String patientHeightChart(HttpSession httpSession, Model model) {
                // retrieve the EHR session from the http session
        Session ehrSession = (Session) httpSession.getAttribute(ehrSessionFactory.getSessionKey());

        Collection<Observation> heightObservations = ehrSession
                .getContextDSTU2()
                .getPatientContext()
                .find(Observation.class, Observation.CODE.exactly().identifier("8302-2"));

        Patient patient = ehrSession.getContextDSTU2().getPatientResource();

        model.addAttribute("title", "DSTU2 Example");

        model.addAttribute("patientFullName", StringUtils.join(patient.getName().get(0).getGiven(), " ") + " " +
                        patient.getName().get(0).getFamily().get(0));

        List<MyHeight> heights = new ArrayList<>();
        for (Observation heightObservation : heightObservations) {
            String date = ((DateTimeDt)heightObservation.getEffective()).getValue().toString();
            String height = ((QuantityDt)heightObservation.getValue()).getValue().toString();
            heights.add(new MyHeight(height, date));
        }

        model.addAttribute("heights", heights);
        return "example";
    }

### Step 3: Display the Results

Using Spring MVC, we display the results calculated into the modelAndView object:

** example.html **

    <h2 th:text="${title}" />

    <h3 th:text="${patientFullName}" />

    <div>
        <table id="observation_list">
            <tr th:each="height : ${heights}">
                <td th:text="${height.date}"/>
                <td th:text="${height.height}"/>
            </tr>
        </table>
    </div>

### Step 4: Try it out

Build the Quick Start:

    mvn clean package

Run the Spring Boot app using either option:

    java -jar target/*.jar --spring.profiles.active=dstu2

or

    java -jar target/*.jar --spring.profiles.active=stu3

Launch the application from a DSTU2 or STU3 sandbox using the following options:

    Client ID: test_client
    Launch URL: http://localhost:8080/launch

